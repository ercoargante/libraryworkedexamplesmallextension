# README #

### What is this repository for? ###


Deze repo bevat een uitwerking van de onderstaande opdracht.

* Het bestand src/main/resources/documentation/ontwerpoverwegingenBsnOpdracht.docx bevat de ontwerpoverwegingen die ten grondslag liggen aan deze oplossing.
* Version 1.0.0.RELEASE
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

## Opdracht: uitbreiding Library Worked Example ##

Maak de volgende modificaties aan het Library Worked Example. Zorg dat de referentie-architectuur gehandhaafd blijft:

1. Voeg aan een Member zijn BSN toe en laat dit ook in de GUI zien. Een BSN moet voldoen aan de 11-proef. De details van de 11-proef zijn te vinden op wikipedia. 
Let op: in het basis worked example heeft de database tabel "member" al een (ongebruikt) veld "Ssn" dat je kunt gebruiken om het BSN in op te slaan. Je mag dit veld overigens ook negeren en je eigen favoriete oplossing kiezen.

2. Voeg de volgende functionaliteit toe: als je via de GUI een lid opzoekt en als dit lid boeken heeft die al ingeleverd hadden moeten worden, meld dit dan in de GUI met rode letters.