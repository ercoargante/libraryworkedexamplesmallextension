/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.avans.library.domain;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Erco
 */
public class DutchSsnTest {

    public DutchSsnTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void validDutchSSN() {
        try {
            new DutchSsn(111222333);
        } catch (InvalidDutchSsnException e) {
            fail();
        }
    }
    
    @Test
    public void invalidDutchSSN() {
        try {
            new DutchSsn(111222332);
            fail();
        } catch (InvalidDutchSsnException e) {
            // deliberately empty
        }
    }
}
