/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.avans.library.datastorage;

import java.sql.ResultSet;
import java.sql.SQLException;
import edu.avans.library.domain.DutchSsn;
import edu.avans.library.domain.InvalidDutchSsnException;

/**
 *
 * @author Erco
 */
public class DutchSsnDAO {

    // prevent instantiation
    private DutchSsnDAO() {
    }

    public static DutchSsn returnDutchSsn(ResultSet resultset) {
        DutchSsn dutchSsn;
        try {
            dutchSsn = new DutchSsn(resultset.getLong("Ssn"));
        } catch (InvalidDutchSsnException | SQLException e) {
            dutchSsn = new DutchSsn();
        }
        return dutchSsn;
    }
}
