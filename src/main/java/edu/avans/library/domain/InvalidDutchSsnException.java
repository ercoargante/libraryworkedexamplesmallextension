/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.avans.library.domain;

/**
 *
 * @author Erco
 */
public class InvalidDutchSsnException extends Exception {
    public InvalidDutchSsnException(String message) {
        super(message);
    }
}
