/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.avans.library.domain;

/**
 *
 * @author Erco
 */
public class DutchSsn {

    private static final int RADIX = 10;
    private static final int ELEVEN = 11;

    private static final long NO_SSN = 0;
    private final long ssn;

    public DutchSsn() {
        this.ssn = NO_SSN;
    }
    
    public DutchSsn(long dutchSsn) throws InvalidDutchSsnException {
        if (isValid(dutchSsn)) {
            this.ssn = dutchSsn;
        } else {
           ssn = NO_SSN;
           throw new InvalidDutchSsnException(dutchSsn + " is not a valid Dutch SSN");
        }
    }

    public long getDutchSsn() {
        return ssn;
    }

    private boolean isValid(long dutchSsn) {
        
        long elevenTest = 0;
        elevenTest += -1 * (dutchSsn % RADIX);
        for (int i = 2; i < 10; i++) {
            dutchSsn = dutchSsn / RADIX;
            elevenTest += i * (dutchSsn % RADIX);
        }

        return elevenTest % ELEVEN == 0;
    }
}
